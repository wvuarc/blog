# GRUSK - 2023

A team of 7 members from the club joined the event. Left Morgantown 1900 h and we arrived at the venue 2330 h.

Members joined

- N0IAN
- WV8EHB
- W8TES
- KB8CFD
- N3AMK
- KA3WVU
- KE8TJE

## Event details

- Operations started with a meeting ~0810 h with organizer (Travis) and locations of the stations were communicated and rides were found to radio operators with different deployement times.
- We had primary/secondary communication channels allocated as follows
	- 146.555 Mhz
	- 146.450 Mhz
	- Spruce knob repeater: 147.285MHz (+) PL 103.5Hz
- stations were established at the designated locations and N3AMK ran the net which was located on the summit

### Log from Net Control

- 1145
	-	N0IAN - Grassy Downhill (EMS Located)
	- Quite a few 84 and 100mi bikers.  No 50 or 33 yet.
	-	W8TES - Late 8th station, nothing to report
	-	WV8EHB - Laurel Forge Campground nothing to report—making hot dogs
	-	KE8TJE - “Widdle” furthest station, no bikes
	-	KB8CFD - Volunteer Fire Station— no bikers
-	1200
	-	N0IAN - more 50 & 33 bikers.  No wipeouts
	-	W8TES - Same.  1 biker, about to head out
	- WV8EHB - few more groups.  Did have to patch someone up—scrape on knee
	-	KE8TJE - nothing still to report
	- KB8CFD - no bikers.  
- 1215
	- KE8TJE - couple came in, 50 mi on their odometer, 100 mi event. 
	-	N0IAN - significant portion of 33/50 mi, and some 80/100 mi.  More to come.  Couple wipeouts, but nothing serious.
	-	W8TES - serving more as guides to get people to the lake, only a couple.
	- WV8EHB - big group of people, not sure what race.  One wipeout
	-	KB8CFD - one stopping by, two or so soon.
- 1225 
	- Chase vehicle left, broken handlebars, will be back
- 1237 
	- KE8TJE, about 20 riders
	- N0IAN - Travis left, curious on fleet vehicle 
	- W8TES - 1 more guy 220 mi.
	- WV8EHB - lost audio signal—relaying through someone else—nothing really.
	- KB8CFD - had so many riders come through—lost count, no injuries.
- 1243 chase vehicle coming through ems station now
- 1300
	- N0IAN - things are quiting down—possibly moving to Corey’s station eventually - eating a nice bagel
	- W8TES - 2 more bikers, still pretty slow
	-	KE8TJE - 40/50 people passed, nothing special 
	- WV8EHB - all good—tandem bike pair, lady riding is blind, which is cool.  
	-	KB8CFD - a LOT of people now.  No incidents, just people.  
- 1320
	- KE8TJE - moved setup, have about 50 people
	- N0IAN— still at grassy downhill, going to move if no one is seen in 10/15 minutes
	-  W8TES - nothing new to add
	- KB8CFD - all good, no reports.
 	- WV8EHB - nothing heard.  
-	1335
	- N0IAN mobile
	-	KE8TJE - 3rd party comms. - (not read—communication abruptly ended)
- 1340
	- N0IAN - Corey’s radio—temp. At side station.  Heading to base shortly
	- W8TES - not really anything.
	- KB8CFD - nothing new.
	- WV8EHB - nothing major - listening to hank jr and chillin.  
	- KE8TJE - no response
- 1400
	- Nothing new all stations

- 1420
	-	N0IAN/W8TES - few more riders coming through.  Jeep Liberty passed to check signage
	-	KB8CFD - consistent amount of people for a good while, on the tail end of it.  2 people on a tandem bike
	-	WV8EHB - nothing at aid station, KE8TJE’s emcomm battery died—on the repeater with handheld.
- 1427 made contact with KE8TJE, audio okay when it works—working on making good contact.
	-	Biker down 6 miles from KE8TJE station Name: Dave Hayes.
	- Tire blown out—mechanical failure—walking bike to somewhere.
	- KE8TJE in low power status, no scheduled checks until better power can be obtained—emergency/priority notice only.
- 1440 WV8EHB - man hurt—needs transport to base camp - arranged transport through Tyson KA3WVU
- 1445 
	- N0IAN - situated at Corey’s location—Corey (W8TES and KA3WVU just left base camp)—nothing new otherwise 
	- WV8EHB - no response.
	- KB8CFD - pace of bikers is slowing, but still coming.  No incidents.
- 1448 
	- N0IAN aide station out of cups for pickle juice 
-	1503–found cups at WV8EHB station to take to N0IAN on the way from WV8EHB station to base camp
-	1514 N0IAN - 80 mi riders going past, 3/4 of 100mi riders.
- 1523 KE8TJE - 407 biker stopping at aide station
- 1547 WV8EHB- biker flat tire between his station and KE8TJE
- 1600 
	- N0IAN - spruce knob lake — quited down, gonna hang.  
	- KB8CFD - taken off—none come in.  Only injury is himself—got stung by yellow jacket 
	- KA3WVU/W8TES Corey possibly seasick.  Nothing else to report.
- 1636 - biker number 185 also needs transport.
- 1705 KB8CFD, starting to pack up—taking Coke to Ian (N0IAN) and then heading back to base camp
- 1743 (N0IAN) - call about a car that has a tire that is in an “iffy” state.  Local guy’s information below:
	- Carl Warner
	- H: 304 567 2617
	-	W: 304 567 2249
	-	Vehicle Tire size:
		- 245/40R19
		- Mercedes-Benz vehicle most likely black convertible type.
	- Carl is on way with rollback truck to obtain vehicle.
- 1804 N0IAN, starting to tear down camp
- Aid station at Whitmore: KE8TJE (updates during lost comms: time at notebook on HF kit)
	- Started operation at 1140 h 
	- Power failure of a Lipo cell. may be due to heating
	- Repeater contact was made with WV8EHB. and Netcontrol was later contacted
	-	ended up getting riders with mechanical failure and this was relayed to net via the repeater. (Transport was requested since from what information was available through the aid station crew that was the standard practice.)
	- Packed up the aid station and left.(with 2 bikers) Notified net about this. (Direct comms was established with a wip via the truck)


## Proposed changes

- The meeting with the organization needs to be more in detail and may be a work sheet can be developed on key points all participating operators know about the backend operation of the bike race. (updated as of latest year changes from the org commite)
- start the net from base station until the summit is up to get information out to stations that are riding long distances.
	- the net formally started 1140 h. We didn't miss any updates since riders will not reach aid stations until 2-3 h from the start of the race at 0900 h, however this could be done earlier.
- Have accurate travel estimates based on local knowledge.
- number aid stations since names are foreign to the operators. (eg. aid x instead of location)  
- Possibility of getting a GMRS license to the club and hand over radios to the following
	- timing station
  - Organizers ( not only Travis, There are key members mostly relatives and friends of Travis who has done it for >5 year (eg. Jason,Heath,Steve,Jan)
 - Place a ham at base with a cross band repeate capable radio. 
- if you are moving on the track keep track of rider no and location you see. This helps with tracking them down.

## Issues seen
	
- changes made to the race back end operations were not propagated to aid stations/operators prior to being deployed
- aid station crews made aware about the course of actions needed. Make it clear they have control over the information sent over radio. The radio operator is there to facilitate information flow.
	-	lack of an actual control body from the organization side. In earlier years there was a hard cut off track close time.
- common information rides asked for and no one knew;
	- how far is the next aid
	- how long are we 
- 70cm antenna attempted to use had a broken Type-N connector.
- Yaesu FT-8900R in bad condition, needing work/repair--possibly replacement


## Changes to the go kit

- Include slim-jims for ease of use
- add a mini voltmeter/ find from the radio
- add a quick reference functions in the kit. Manual is there but it takes time to find what is needed.
- include a multi-tool to the kit
